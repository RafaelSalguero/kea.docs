
#Comandos 
Los nombres de los comandos no deberán de tener el posfijo `Command`

Utilice la clase `AsyncCommand` para definir todos los comandos, incluso los que no son asíncronos.
Las propiedades de comandos deben de ser de tipo `IAsyncCommand`

```java
/// <summary>
/// Procesa los cambios. Si se lanzan excepciones amistosas muestra un mensaje al usuario
/// </summary>
public IAsyncCommand Process => new AsyncCommandException(exceptionHandler, async o =>
{
    await SaveIntern();
}, o =>CanEdit);


/// <summary>
/// Cierra la ventana
/// </summary>
public IAsyncCommand Close => new AsyncCommand(o => TryClose());
```

##Comandos secuenciales
Utilice la función `Then` para crear un comando despues de otro, este nuevo comando respetara la reentrada y los `CanExecute` de cada comando
```
public IAsyncCommand Save => ...
public IAsyncCommand Close => ...

/// <summary>
/// Guarda y cierra la ventana
/// </summary>
public IAsyncCommand SaveClose => Save.Then(Close);
```

##Lock
Los comandos asíncronos no permiten la reentrada, si quiere tener un control mas estricto puede utilizar la función `Lock` para poner 
un lock exclusivo para cierto comando

```
readonly object saveLock = new object();

/// <summary>
/// Guarda los cambios en los permisos
/// </summary>
public IAsyncCommand Save => new AsyncCommand(async o =>
{
    ...
}).Lock(saveLock);

/// <summary>
/// Guarda los cambios en los permisos y cierra la ventana
/// </summary>
public IAsyncCommand SaveClose => Save.Then(Close);
```

#Selectores
Cuando en un view model de edición necesite seleccionar un elemento de una tabla utilice los view models de lista para hacer binding 
a los selectores de la UI.

```java
//El modelo Cliente tiene una colonia
public class ClienteEditViewModel : BaseEditViewModel<Cliente, int>
{
    public ClienteEditViewModel(BaseEditDependencies db, ColoniaListViewModel colonias) : base(db)
    {
        this.Colonias = colonias;
    }

    public ColoniaListViewModel Colonias { get; private set; }
}
```

```
<ComboBox 
ItemsSource="{Binding Colonias.Items}" 
SelectedValue="{Binding Model.IdColonia}"
SelectedValuePath="IdRegistro"
DisplayMemberPath="Nombre" />
```