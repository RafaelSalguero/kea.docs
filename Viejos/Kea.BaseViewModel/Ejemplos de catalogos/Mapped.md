# Mapped
- El tipo del modelo de cada item no es el mismo que el tipo de entidad de la base de datos
- El view model de edición debe de implementar `IInsertUpdateViewModel<T>`
- El mapeo extrae la llave primaria del item, y carga el modelo de la base de datos.
- Se debe de poder extrar la llave primaria al item, lo mas común para lograr esto es que el tipo
de item herede del tipo de entidad

## Listado
```c#
public class ClienteListViewModel : BaseDbListViewModel<ClienteListModel, int?, ClienteListViewModel.FiltroList, Db>
{
    public ClienteListViewModel(
        BaseListDeps<Db> deps,
        QueryService queryService,
        ListService listService,
        PermisosCliente permisos) :
        base(deps,
            queryService,
            listService,
            permisos)
    {
        this.Permisos = permisos;
        this.DisplayName = "Clientes";
    }

    /// <summary>
    /// List service del cliente. Heredamos de SimpleDbMappedListService
    /// </summary>
    public class ListService : SimpleDbMappedListService<Cliente, ClienteListModel, ClienteEditViewModel, Db>
    {
        public ListService(Func<Db> context, IWindowManager windowManager, Func<Task<ClienteEditViewModel>> editViewModelFactory) : base(context, windowManager, editViewModelFactory)
        {
        }
    }

    /// <summary>
    /// Contiene los permisos de la presente ventana
    /// </summary>
    readonly PermisosCliente Permisos;

    /// <summary>
    /// Función que crea un cliente edit
    /// </summary>
    readonly Func<Task<ClienteEditViewModel>> crearEdit;

    [RefreshNow]
    public string FiltroNombre { get; set; }

    /// <summary>
    /// Parametros del filtro
    /// </summary>
    [ImplementPropertyChanged]
    public class FiltroList
    {
        public string Nombre { get; set; } = "";
    }

    public class QueryService : Kea.BaseLogic.Services.Query.DbFilterMapService<Cliente, ClienteListModel, FiltroList, Db>
    {
        public QueryService(ClienteConsultaController controller)
        {
            this.controller = controller;
        }
        readonly ClienteConsultaController controller;

        public override async Task<IQueryable<Cliente>> Filter(Db c, FiltroList filter)
        {
            var result = await base.Filter(c, filter);

            var filtroNombreSinEspaciosMinusculas = filter.Nombre.Replace(" ", "").ToLower();
            result = result.Where(x => (x.Nombre + x.Apellido1 + x.Apellido2)
                                    .ToLower()
                                    .Replace(" ", "")
                                    .Contains(filtroNombreSinEspaciosMinusculas));

            return result;
        }

        public override async Task<IReadOnlyList<ClienteListModel>> Map(Db c, IQueryable<Cliente> set)
        {
            return await controller.ObtenerClientesListModel(c, set);
        }
    }
}
```

## Edición
Exactamente igual que `Simple`