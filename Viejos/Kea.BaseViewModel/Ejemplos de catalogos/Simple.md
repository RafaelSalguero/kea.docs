# Catálogo simple
- Los catalogos estan formados siempre por 2 vistas, la de listado
y la de edición.
- Para catalogos simples:
  - Los elementos del listado son del mismo tipo que 
el tipo de entidad
  - El view model de edición no necesita parametros del listado
  - El view model de edición debe de implementar `IInsertUpdateViewModel<T>`. Se puede usar al
`BaseDbEditViewModel` para lograr esto

- En la vista del listado haga binding a:
  - Los comandos `New`, `Edit`, `Remove`
  - Los parametros del filtro si es que tiene un filtro, estos estan en la propiedad `State`
  - A `Items` y `SelectedItem` ya sea en un `ListView` o en un `DataGrid`
  - Al comando `ItemActivate` usando el `ItemActivationBehaviour`

- Diseño de la vista de edición:
  - Siempre utilice `ui:AutoGridN` cuando sea posible
  - Divida con un `Grid` de dos filas el area de los botones 
  y de edicion de la forma

```xml
<!-- xmlns:ui="http://toniccomputing.com/patterns/ui" -->
<Grid>
    <!--2 filas-->
    <Grid.RowDefinitions>
        <RowDefinition Height="Auto" />
        <RowDefinition Height="Auto" />
    </Grid.RowDefinitions>

    <!--pseudocódigo -->
    <ui:AutoGridN Grid.Row="0" >
        <TextBlock Text="Campo 1:" />
        <TextBox Text="{Binding Campo1}" />

        <!-- otros campos aqui ... -->
    </ui:AutoGridN />

    <Botones Grid.Row="1" />
</Grid>
```

- Diseño de la vista de listado:
  - Divida con un `Grid` de 3 filas los filtros, botones, y el listado. Ej:
```xml
<Grid>
    <!--3 filas-->
    <Grid.RowDefinitions>
        <RowDefinition Height="Auto" />
        <RowDefinition Height="Auto" />
        <RowDefinition Height="*" />
    </Grid.RowDefinitions>

    <!--pseudocódigo -->
    <Filtros Grid.Row="0" />
    <Botones Grid.Row="1" />
    <Listado Grid.Row="2" />
</Grid>
```
## Listado
### View model
```java
public class EjidoListViewModel : BaseSimpleDbListViewModel<Ejido, int?, EjidoEditViewModel, EjidoListViewModel.FiltroList, Db>
{
    public EjidoListViewModel(
        BaseSimpleDbDeps<EjidoEditViewModel, Db> deps,
        QueryService queryService,
        PermisosEjido permisos) :
        base(deps, queryService, permisos)
    {
        this.DisplayName = "Ejidos";
    }

    /// <summary>
    /// Filtro para el listado de la entidad <see cref="Ejido"/>
    /// </summary>
    [ImplementPropertyChanged]
    public class FiltroList
    {
        /// <summary>
        /// Propiedad para filtrar por nombre las entidades Ejido
        /// </summary>
        public string Nombre { get; set; }
    }

    /// <summary>
    /// Query service para la entidad <see cref="Ejido"/>
    /// </summary>
    public class QueryService : DbQueryService<Ejido, FiltroList, Db>
    {
        /// <summary>
        /// Método para filtrar las entidades
        /// </summary>
        /// <param name="c"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public override async Task<IQueryable<Ejido>> Filter(Db c, FiltroList filter)
        {
            var result = await base.Filter(c, filter);

            //En este lugar deben ponerse los if que filtrarán las entidades
            if (!String.IsNullOrWhiteSpace(filter.Nombre))
                result = result.Where(x => x.Nombre.ToLower().Contains(filter.Nombre.ToLower()));

            return result.OrderBy(x => x.Nombre);
        }
    }
}
``` 

### View 

```xml
 <Grid>
    <Grid.RowDefinitions>
        <RowDefinition Height="auto"></RowDefinition>
        <RowDefinition Height="*"></RowDefinition>
        <RowDefinition Height="auto"></RowDefinition>
    </Grid.RowDefinitions>

    <GroupBox Header="Filtro" Grid.Row="0" DataContext="{Binding State}">
        <ui:AutoGridN>
            <TextBlock>Descripción</TextBlock>
            <TextBox Text="{Binding Descripcion, UpdateSourceTrigger=PropertyChanged, Delay=100}" />
        </ui:AutoGridN>
    </GroupBox>

    <Grid Grid.Row="1">
        <Grid.RowDefinitions>
            <RowDefinition Height="Auto" />
            <RowDefinition Height="*" />
        </Grid.RowDefinitions>

        <Border Style="{StaticResource ButtonsBorder}" Grid.Row="0">
            <UniformGrid HorizontalAlignment="Left" Rows="1">
                <Button Command="{Binding New}"  >Crear</Button>
                <Button Command="{Binding Edit}" >Editar</Button>
                <Button Command="{Binding Remove}" >Borrar</Button>
            </UniformGrid>
        </Border>

        <ListView ItemsSource="{Binding Items}" SelectedItem="{Binding SelectedItem}" Grid.Row="1"
                    base2:ItemActivationBehaviour.Command="{Binding ItemActivate}">
            <ListView.View>
                <GridView>
                    <GridView.Columns>
                        <GridViewColumn  Header="Abreviatura del cultivo" DisplayMemberBinding="{Binding AbreviaturaCultivo}" Width="auto" />
                        <GridViewColumn  Header="Descripción" DisplayMemberBinding="{Binding Descripcion}" Width="auto" />
                    </GridView.Columns>
                </GridView>
            </ListView.View>
        </ListView>
    </Grid>

    <Border Style="{StaticResource ButtonsBorder}" Grid.Row="2">
        <UniformGrid HorizontalAlignment="Right" Rows="1">
            <Button Command="{Binding Close}"  >Cerrar</Button>
        </UniformGrid>
    </Border>
</Grid>
```

## Edición
### View model
```java
public class EjidoEditViewModel : BaseDbEditViewModel<Ejido, Db>
{
    public EjidoEditViewModel(BaseDbEditDependencies<Db> deps, PermisosEjido permisos)
        : base(deps, permisos)
    {
        this.Permisos = permisos;
    }

    /// <summary>
    /// Contiene los permisos de la presente ventana
    /// </summary>
    readonly PermisosEjido Permisos;

    public override string DisplayName
    {
        get
        {
            return Editando ? "Editar ejido" : "Nuevo ejido";
        }
        set
        {

        }
    }
}
```

### View
```xml
 <Grid>
    <Grid.RowDefinitions>
        <RowDefinition Height="*" />
        <RowDefinition Height="Auto" />
    </Grid.RowDefinitions>

    <ui:AutoGridN ChildVerticalAlignment="Center" Margin="10">

        <TextBlock>Abreviatura cultivo</TextBlock>
        <TextBox Text="{Binding Model.AbreviaturaCultivo}" />

        <TextBlock>Descripción</TextBlock>
        <TextBox Text="{Binding Model.Descripcion}" />

        <TextBlock>Cuenta contable</TextBlock>
        <DockPanel LastChildFill="True">
            <Button Command="{Binding AbrirVentanaSeleccionCuentaContable}" DockPanel.Dock="Right" Content="..." Margin="0"></Button>
            <TextBox IsReadOnly="True" DockPanel.Dock="Left" Text="{Binding CuentaContableSeleccionada.Descripcion}"  />
        </DockPanel>

    </ui:AutoGridN>

    <Border Style="{StaticResource ButtonsBorder}" Grid.Row="1">
        <UniformGrid HorizontalAlignment="Right" Rows="1">
            <Button Command="{Binding SaveClose}"  >Guardar y cerrar</Button>
            <Button Command="{Binding Close}"  >Cerrar</Button>
        </UniformGrid>
    </Border>
</Grid>
```