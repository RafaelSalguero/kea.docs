# Libreria para aplicaciones de WPF
Libreria modular basada en el patrón de composición para facilitar 
el desarrollo de catalogos.
## `Kea.BaseLogic`
Interfaces y los servicios básicos. Este nuget no
tiene dependencia con Entity Framework.


## `Kea.BaseLogic.EF6`
Implementaciones de las interfaces básicas para Entity Framework.

Incluye el `BaseEditService` que es la implementación por default del 
`IInsertUpdateService` la cual implementa validaciones de entidad usando
el `Kea.Validation`

## `Kea.BaseViewModel`
Contiene los view model fundamentales para las ventanas de listados y edición

### BaseEditViewModel
Edición de un modelo sin el concepto de modo de edición o creación

### BaseInsertUpdateViewModel
Edición o creación de un modelo, tiene el concepto del modo de edición.
Implementa a `IInsertUpdateViewModel<T>` la cual lo hace compatible con algunos
servicios como:

- SimpleDbMappedListService
- SimpleDbListService
- SimpleDbListInsertUpdateService
- SimpleListInsertUpdateService
- LoaderListInsertUpdateService


### BaseQueryViewModel
Consultas de solo lectura. Depende de un `IPureQueryService<T>` el cual
provee del `IReadOnlyList<T>` que será mostrado al usuario.

Algunas implementaciones del `IPureQueryService`:
- DbQueryService
- DbFilterMapService
- InMemoryQuery

Tiene el concepo de un `State` el cual es un objeto al cual se le hace un `DeepWatch` 
que ocaciona un regenerado del listado cada que cambia cualquiera de sus propiedades.

Maneja la sincronización de `SelectedItem` y `SelectedItemId` lo cual nos permite hacer
binding a cualquiera de la que sea conveniente.

Expone el `OnSelectedEntity` el cual devuelve un `IObservable<T>` con la entidad que 
le corresponde al `SelectedItemId` cada que cambia. Nos permite anidar selectores entre sí.


### BaseListViewModel
Consultas de edición. Hereda de `BaseQueryViewModel`. Depende de un `IListService` el cual
determina la acción que se ejecutará cuando el usuario quiera crear, editar, o borrar una entidad 
de la lista.

Algunas implementaciones del `IListService`
- SimpleDbListService
- SimpleDbMappedListService
- DbMappedListService
- MappedListService
- ComposedListService

## `Kea.BaseViewModel.EF6`
Contiene view models preconfigurados con los servicios para Entity Framework y algunas 
implementaciones de interfaces de `Kea.BaseViewModel` para entity Framework

### BaseDbEditViewModel
View model para la edición de una entidad de la base de datos. Este view model no tiene 
lógica propia si no que simplemente hereda a `BaseEditViewModel` y lo preconfigura con los 
servicios:

|Implementación       |Parametro del constructor|Interfaz
|----                 | ----                    |---
|`NewEntityService<T>`|model init               |`ILoadEntityService<T>`
|`DbReloadService<T>` |reload model             |`IFindEntityService<T,T>`
|`DbSaveChanges<T>`   |save changes             |`IContextSaveChanges`
|
|`BaseEditService<T>` |insert update service    |`IInsertUpdateService`