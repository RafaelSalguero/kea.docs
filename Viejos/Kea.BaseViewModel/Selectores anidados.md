# Selectores anidados
Si se quiere que la seleccion de un selector sea el filtro de otro selector:
- Ocupara tener un parametro en el filtro que realice este filtrado en los view models hijos
- Ocupara un view model padre compuesto de todos los listados que participan en la jerarquía.
- Utilice `Kea.Bind` en conjunto con `OnSelectedEntity()` para actualizar 
al padre seleccionado dado un hijo seleccionado
- Utilice a `Kea.Bind` a la propiedad `SelectedItemId` del listado padre para actualizar el filtro
del listado hijo
- Estos bindings se pueden anidar para representar una relacion de cualquier cantidad de niveles

Ejemplo relacion estado-municipio:

**UbicacionesViewModel (constructor)**

```java
//Actualiza el filtro del municipio cada que cambie el estado seleccionado
Bind.OnChange(() => this.Estados.SelectedItemId)
.Subscribe(x => this.Municipios.State.IdEstado = x);

//Cuando cambie el municipio seleccionado, actualiza el IdEstado
Municipios.OnSelectedEntity()
.Where(x => x != null) //Si el usuario no selecciono nada, no actualizamos al padre
.Select(x => x.IdEstado) //x es un municipio, pero solo nos interesa el IdEstado, 
.Subscribe(id => this.Estados.SelectedItemId = id); //Cambiamos el estado seleccionado
```

**View model que consuma a UbicacionesViewModel**

```java
//Un modelo que tiene IdMunicipio hace binding directamente al SelectedItemId del municipio
//No es necesario almacenar el IdEstado en el modelo
Kea.Bind.TwoWay(()=> this.Ubicaciones.Municipios.SelectedItemId, ()=> this.Model.IdMunicipio)
```

**Ejemplo completo de un view model de ubicaciones**
La jerarquía de ubicaciones de esta clase consiste en:
- `Paises`, la cual afecta al ListViewModel de -> `Estados`
- `Estados`, la cual afecta al ListViewModel de -> `Municipios`
- `Municipios`, la cual afecta los ListViewModel de -> `Localidades` y `Colonias`

```java
/// <summary>
/// Jerarquia de paises, estados, municipios, colonias y localidades
/// </summary>
public class UbicacionesViewModels
{
    public UbicacionesViewModels(EstadoListViewModel estados,
        MunicipioListViewModel municipios, CiudadListViewModel localidades, ColoniaListViewModel colonias)
    {
        this.Estados = estados;
        this.Municipios = municipios;
        this.Ciudades = localidades;
        this.Colonias = colonias;


        Bind.OnChange(() => this.Estados.SelectedItemId)
            .Subscribe(x =>
            {
                this.Municipios.State.IdEstado = x;
            });

        Municipios.OnSelectedEntity()
            .Where(x => x != null)
            .Select(x => x.IdEstado)
            .Subscribe(id =>
            this.Estados.SelectedItemId = id);

        Bind.OnChange(() => this.Municipios.SelectedItemId)
            .Subscribe(x =>
            {
                this.Colonias.State.IdMunicipio = x;
                this.Ciudades.State.IdMunicipio = x;
            });

        ///Es importante mencionar que tanto el cambio de colonia como de ciudad puede actualizar el idMunicipio
        this.Colonias.OnSelectedEntity()
            .Where(x => x != null)
            .Select(x => x.IdMunicipio)
            .Subscribe(id =>
            {
                this.Municipios.SelectedItemId = id;
            });

        this.Ciudades.OnSelectedEntity()
            .Where(x => x != null)
            .Select(x => x.IdMunicipio)
            .Subscribe(id =>
            {
                this.Municipios.SelectedItemId = id;
            });
    }

    /// <summary>
    /// Listado de estados que están en el país seleccionado
    /// </summary>
    public EstadoListViewModel Estados { get; set; }

    /// <summary>
    /// Listado de municipios que están en el estado seleccionado
    /// </summary>
    public MunicipioListViewModel Municipios { get; set; }

    /// <summary>
    /// Listado de localidades que se encuentran en el municipio seleccionado
    /// </summary>
    public CiudadListViewModel Ciudades { get; set; }

    /// <summary>
    /// Listado de colonias que se encuentran en el municipio seleccionado
    /// </summary>
    public ColoniaListViewModel Colonias { get; set; }
}
```