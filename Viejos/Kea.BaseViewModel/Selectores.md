# Selectores
Para que un view model tenga un selector de elementos de un listado,
utilice el `ListViewModel` del listado:

*Ejemplo: La edición del cliente tiene un selector de su ocupación*

```java
public class ClienteEditViewModel 
{
    public ClienteEditViewModel(OcupacionListViewModel ocupacionList)
    {
        this.Ocupaciones = ocupacionList;

        //Ligamos el elemento seleccionado del OcupacionList con el de nuestro modelo,
        //esto es opcional pero es la manera mas sencilla de ligar la seleccion de un listado a un modelo
        Kea.Bind.TwoWay(() => Ocupaciones.SelectedItemId, () => Model.IdOcupacion);
    }

    //Haremos binding a esta propiedad desde la vista para los 
    //selectores
    public OcupacionListViewModel Ocupaciones { get; private set; }
}
```

## Inline

```xml
<ComboBox 
ItemsSource="{Binding Ocupaciones.Items}" 
SelectedItem="{Binding Ocupaciones.SelectedItem}" />
```
Las propiedades `Items` y `SelectedItem` estan definidas en el `BaseQueryViewModel` del cual heredan
todos los view model de listados, ademas estas se encuentran en las interfaces `IHasItems<T>` y `ISelector<T>`

## Con ventana

```xml
<DockPanel LastChildFill="True">
        <Button 
            Command="{Binding Ocupaciones.MostrarSelector}" 
            DockPanel.Dock="Right" 
            Content="..." 
            Margin="0" />

        <TextBlock Text="{Binding Ocupaciones.SelectedItem}" />
</DockPanel>
```

En este caso hacemos binding a `SelectedItem` y al comando `MostrarSelector`.
Este comando esta definido tambien en el `BaseQueryViewModel` y su función es 
mostrar una ventana con el view model selector.