
#Menu principal
##Atributo `MenuElement`
El menu principal de la aplicación es generado automaticamente segun la estructura de las clases y los espacios de nombres de las mismas

Para que una clase aparezca como un elemento en el menu principal márquela con el atributo `MenuElement`

Si la clase debe de ocultarse condicionalmente segun el rol del usuario utilize una clase que herede de `PermisoMenu`, esta clase contiene 
una sola propiedad booleana que determina si se mostrara el elemento en el menu

Utilice el atributo `[Description]` para establecer la descripción amistosa para el usuario

Para establecer un titulo diferente al menu del nombre de la clase utilice la propiedad `Title`

```
//Indicamos que el tipo de permisos que determina 
//si la clase se va a mostrar o no es PermisosACuentaDe
[Description("Catálogo de cuentas")]
[MenuElement(typeof(PermisosACuentaDe), Title = "A cuenta de")]
public class ACuentaDeViewModel 
{ 
//...
```

##Acciónes personalizadas del menu
Implemente la interfaz `IMenuAction` para ejecutar un método en lugar del comportamiento por default que es 
llamar al `windowManager.ShowWindow(instance)` donde `instance` es la instancia recien construida de la clase marcada con el 
atributo.

```
[MenuElement]
public class CrearMinutaMenuAction : IMenuAction
{
	//Inyectamos una función constructora del MinutaEditViewModel
	public CrearMinutaMenuAction(Func<Task<MinutaEditViewModel>> crearMinutaEdit)
	{
		this.crearMinutaEdit = crearMinutaEdit;
	}

	readonly Func<Task<MinutaEditViewModel>> crearMinutaEdit;

	//Este método se ejecutara cuando el usuario entre a esta opción del menu principal
	public async Task MenuAction()
    {
        if(PuedeEntrar())
			windowManager.ShowWindow(await crearMinutaEdit());
		else
			await windowManager.ShowSelectorAsync(new ErrorViewModel());
    }

```
