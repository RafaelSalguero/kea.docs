# Persistence

Este proyecto debe de contener solamente codigo de acceso a datos. La lógica de negocios no debe de ir aqui

La cadena de conexión esta configurada en el ejecutable
`DatabaseSkeleton/App.config`

## Agregar una migración
Ejecute el comando `Add-Migration myMigrationDescription` en la consola de paquetes NuGet

## Convenciones para los modelos
### Nombres
Los nombres de las clase deben de estar en singular
```
public class Municipio { ... }
```
### Notificación de cambio de propiedad
Todos los modelos deben de agregar el atributo `ImplementPropertyChanged`

Implementar la interfaz `INotifyPropertyChanged` manualmente no es necesario

```
[ImplementPropertyChanged]
public class Municipio{
	//Propiedades ...
}
```

### Implementar `ToString()`
Todas las entidades deberán de implementar el método `ToString` a menos que la entidad no tenga una representación sencilla en texto

### Comentarios
Todas las columnas deben de tener comentarios

### Llaves primarias
Todas las llaves primarias se deben de llamar `IdRegistro` y ser de tipo `int`

``` 
/// <summary>
/// Llave primaria
/// </summary>
[Key]
public int IdRegistro { get; set; }
```
### Llaves foráneas
Todas las llaves foráneas estarán definidas por dos propiedades, la propiedad de columna debe de estar primero
seguida de la propiedad de navegación.

La propiedad de navegación no debe de ser virtual

Se marcará con el atributo `ForeignKey` a la propiedad de columna
```
/// <summary>
/// Id del estado al ue pertenece el municipio
/// </summary>
[ForeignKey(nameof(Estado))]
public int IdEstado { get; set; }

/// <summary>
/// Estado al que pertenece el municipio
/// </summary>
public Estado Estado { get; set; }
```

### Propiedades de navegación de coleccion
No deberán de estar marcadas como `virtual` y deberan de ser de tipo `ICollection<T>`. No es
obligatorio que todas las relaciones tengan una propiedad de navegación de colección

Deberán de tener el atributo `InverseProperty`

```
public class Estado 
{

	/// <summary>
	/// Municipios dentro del estado
	/// </summary>
	[InverseProperty(nameof(Municipio.Estado))
	public ICollection<Municipio> Municipios { get; set; } = new HashSet<Municipio>();

}
```

## Relaciones
### Relaciones `one-to-zero-one` 

En la tabla secundaria la llave primaria sera tambien la llave foránea a la secondaria y esta tendra nomenclatura
de llave foránea:

```
public class FotoPerfil
{
    /// <summary>
    /// Llave primaria y IdPersona de esta foto de perfil
    /// </summary>
    [Key]
    [ForeignKey(nameof(Credito))]
    public int IdPersona { get; set; }

    /// <summary>
    /// La persona de esta foto de perfil
    /// </summary>
	public Persona Persona { get; set; }
}
```

En la primaria se puede poner opcionalmente la propiedad de navegación:

```

public class Persona 
{

	/// <summary>
    /// Foto de perfil de esta persona
    /// </summary>
	[InverseProperty(nameof(FotoPerfil.Persona))]
	public FotoPerfil Foto { get; set; } 
}
	
```

### Relaciones `many-to-many`
Siempre hay que usar relaciones many to many explicitas
```
//Puntos de la relacion: Miembro <-> Comite

public class Comite
{
	/// <summary>
	/// Miembros del comite
	/// </summary>
	[InverseProperty(nameof(MiembroComite.Comite))]
	public ICollection<MiembroComite> MiembrosComite { get; set; } = new HashSet<MiembroComite>();
}

public class Miembro 
{
	/// <summary>
	/// Miembros del comite
	/// </summary>
	[InverseProperty(nameof(MiembroComite.Miembro))]
	public ICollection<MiembroComite> MiembrosComite { get; set; } = new HashSet<MiembroComite>();
}

//Tabla explicita
public class MiembroComite 
{
	[Key, Column(Order = 0)]
	[ForeignKey(nameof(Comite))]
	public int IdComite { get; set; }
	
	public Comite Comite { get; set; }

	[Key, Column(Order = 1)]
	[ForeignKey(nameof(Miembro))]
	public int IdMiembro { get; set; }

	public Miembro Miembro { get; set; }
}
```