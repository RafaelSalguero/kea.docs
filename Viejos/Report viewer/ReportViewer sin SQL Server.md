# Problemas con ReportViewer

Para correr un reporte de ReportViewer de WinForms en una computadora sin SQL Server:

Instalar `chocolately` (en powershell:)
```
Set-ExecutionPolicy -ExecutionPolicy RemoteSigned
iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
```

Instalar `Sql Server 2014 CLR Types`
- choco install sql2014.clrtypes

Instalar `ReportViewer 2015.msi`
- El archivo esta adjunto a este repositorio