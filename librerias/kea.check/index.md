# kea-check

Un programa de consola que verifica que los archivos en producción encajen con los
archivos de desarrollo, manteniendo el archivo "check.json".

Por ahora sólo los proyectos de ASP.NET con postgres estan soportados.

## Objetivos
- Verificar la integridad de los proyectos 
- Forzar ciertas politicas de publicación
- Correr migraciones

## Instrucciones

### Preparar el proyecto
- Verifique que la solución del proyecto puede compilar tanto en modo Debug como en Release
- Agregue el ejecutable `kea-check.exe` al proyecto principal, en la carpeta del archivo `.csproj` que se va a publicar
- En el archivo `.csproj`, agregar el `Deterministic` en el primer `PropertyGroup`, esto para que cada compilación genere los mismos binarios para el mismo código y configuración:
```xml
<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="12.0" DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
    <PropertyGroup>
        <!--Agregar este elemento: -->
        <Deterministic>true</Deterministic>
    </PropertyGroup>
</Project>
```
- En el proyecto de persistencia:
    1.- El `Db` debe de estar configurado por medio del código, para esto cree la clase `DbCofig`
    ```csharp
    public class DbConfig : DbConfiguration
    {
        public DbConfig()
        {
            var provider = "Npgsql";
            SetProviderFactory(provider, NpgsqlFactory.Instance);
            SetProviderServices(provider, NpgsqlServices.Instance);
            SetDefaultConnectionFactory(new NpgsqlConnectionFactory());

        }
    }

    //En la clase Db:
    [DbConfigurationType(typeof(DbConfig))]
    public class Db : DbContext
    {
        //...
    }
    ```

    2.- Tener el método `RunMigrations` en la clase `Db`, este método será ejecutado por el programa:
    ```csharp
    public static void RunMigrations(string cs)
    {
        Database.SetInitializer(new MigrateDatabaseToLatestVersion<Db, Migrations.Configuration>(true));
        using (var c = new Db(cs))
        {
            c.Database.Initialize(true);
        }
    }
    ```

### Crear una nueva versión
- Antes de publicar el proyecto aumente el número de versión en el atributo `AssemblyVersion`, ya que el kea-check verifica que la nueva versión sea mayor a la versión anterior
- Cree el commit que se va a publicar, ya que el programa no permite crear una nueva versión si hay cambios pendientes en el git
- Corra el programa, una vez terminado el proceso, se habrá creado el archivo `check.json`, el cual contiene un historia con todas las versiones del sistema, y un commit donde se incluye a este archivo
- Agregue este archivo al proyeto para que sea copiado junto con la publicación, poner `CopyAlways` para que quede en el bin

### Publicar
- Una vez terminada la publicación ejecute de nuevo el programa en la carpeta publicada
- El programa verificará la integridad de todos los archivos, incluyendo el `Web.Config`, y correrá las migraciones pendientes
- En caso de detectar algun problema de integridad el programa indica que archivo o migración es la que ocasionó un problema
