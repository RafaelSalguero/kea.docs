# Sistema de tickets
## Objetivos
- Evitar problemas con los clientes formalizando sus requerimientos, dando a los mismos un folio y correo para su seguimiento
- Justificar nuestro trabajo y cobranza a los clientes, dando un listado detallado de todo lo trabajado en cierto periodo de facturación
- Controlar los pendientes de desarrollo y publicaciones de tal manera que nunca se dejen errores o desarrollos sin atención
- Formar una base de datos de bugs

## Pasos

### Responsabilidad de la persona que recibe el requerimiento / encuentre un bug
- Crear el ticket
- Para tickets que provengan de un requerimiento de un cliente: Enviar un correo al cliente con el folio del ticket
- El ticket será asignado al desarrollador que resolverá el ticket

### Responsabilidad del desarrollador:
- No realizar ningún desarrollo sin un ticket, todo desarrollo debe de tener uno sin excepción
- Detallar la solución del ticket usando las notas. Ej. como se resolvió el bug, explicación del origen del bug, etc...
- Al estar resuelto el ticket agregar una nota indicando que ya se resolvió y que esta pendiente de publicar
- Asegurarse que su desarrollo terminado fue publicado, incluso si el mismo no va a ser quien publique

### Responsabilidad de la persona que publique el ticket:
- Cerrar el ticket después de publicar
- Notificar al cliente cuando el ticket este cerrado por medio de un correo, indicando el folio del mismo, con copia al desarrollador

### Otros
- En caso de que un ticket se encuentre repetido, deshabilitar el ticket mas viejo, agregando una nota indicando que el seguimiento del ticket se llevara en el nuevo ticket, indicando su folio

### Ticket predeterminado para agregar en el sistema:
- En todos los casos sin excepción se levantará el ticket de "Informática", esto para poder asignar a ingeniero que se requiere en el mismo.