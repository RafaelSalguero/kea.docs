# Sistema de nomina

## Objetivo

### Nómina aleatoria / trabajadores eventuales de campo / TEC
Llevar el control de la nómina aleatoria o de eventuales. Dado cierto monto a repartir en un periodo de nómina, el sistema elige a los trabajadores de forma aleatoria y realiza los calculos de sueldo y destajo según las fórmulas inventadas por el contador para ahorrar impuestos. El resultado del proceso es un conjunto de nóminas las cuales son enviadas al SAT (por medio del timbrado) y al SUA e IDSE (por medio de reportes de CSV).

Este tipo de nómina sólo aplica para las empresas que tienen jornaleros.

### Nómina administrativa
Calcular la nómina de multiples empresas según los movimientos afilatorios de los trabajadores, esta es la nómina "normal"

